# A debhelper build system class for handling grunt based projects.
#
# Copyright: © 2017 Michael Biebl
# License: GPL-2+

package Debian::Debhelper::Buildsystem::grunt;

use strict;
use warnings;
use parent qw(Debian::Debhelper::Buildsystem);

use constant BUILD_CMD => 'grunt';

sub DESCRIPTION {
	"grunt (Gruntfile.js / Gruntfile.coffee)"
}

sub new {
	my $class=shift;
	my $this=$class->SUPER::new(@_);
	# If this build system *requires* running the tool in the same directory
	# as the source, then use:
	#   $this->enforce_in_source_building();
	# On the other hand, if it must be run in a seperate build dir (a la meson),
	# then use:
	#   $this->prefer_out_of_source_building(@_);
	# If neither applies, this sub can simply be removed
	return $this;
}

sub check_auto_buildable {
	my $this=shift;
	my ($step) = @_;

	if (-e $this->get_buildpath('Gruntfile.js') ||
		-e $this->get_buildpath('Gruntfile.coffee')) {
		return 1;
	}
	return 0;
}

sub build {
	my $this=shift;

	# Passing default options can be done via something like:

#	if ($this->get_parallel() > 0) {
#		unshift @_, "-j" . $this->get_parallel();
#	}
	$this->doit_in_builddir(BUILD_CMD, 'build', @_);
}

sub configure {
	# Do nothing
}

sub test {
	# Do nothing
}

sub install {
	# Do nothing
}

sub clean {
	my $this=shift;
	# If we are using a separate build dir, then just remove the build directory
	if (!$this->rmdir_builddir()) {
		# Otherwise, run the tool with "clean"
		$this->doit_in_builddir(BUILD_CMD, 'clean', @_);
	}
}

1
